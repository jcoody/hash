#!/usr/bin/env node

var packageJSON = require('./package.json'),
		program = require('commander');

program.version(packageJSON.version)
	.usage('<string> [options]')
	.option('-c, --characters <value>', 'Specify how many characters are in string', parseInt)
	.option('-d, --dehash', 'Dehash the provided string')
	.option('-k, --key <value>', 'Specify the key to be used in hash')
	.parse(process.argv);

var characters = program.characters ? program.characters : 7,
		key = program.key ? program.key : "acdegilmnoprstuw",
		string = program.args[0];

if(program.dehash) {
	var array = [string],
			word = "";
	for(var i = characters; i >= 0; i--) {
			string = Math.round(string / 37);
			array.unshift(string);
	}
	for(var i = 0; i < array.length-1; i++) {
		word = word + key[array[i+1] - array[i] * 37];
	}
	console.log(word);
} else {
	hash = characters;
	for(var i = 0; i < string.length; i++) {
		hash = hash * 37 + key.indexOf(string[i]);
	}
	console.log(hash);
}
